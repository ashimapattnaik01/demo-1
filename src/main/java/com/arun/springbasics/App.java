package com.arun.springbasics;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.arun.springbasics.configuration.AppConfig;
import com.arun.springbasics.javaconfig.Dance;
import com.arun.springbasics.javaconfig.Music;
import com.arun.springbasics.xmlconfig.BookTicket;
import com.arun.springbasics.xmlconfig.Car;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	/*
    	ClassPathXmlApplicationContext context =
    			new ClassPathXmlApplicationContext
    			0("spring-web.xml");
    	*/
    	AnnotationConfigApplicationContext ctx =
    			new AnnotationConfigApplicationContext();
    	ctx.register(AppConfig.class);
    	ctx.refresh();
    	Dance dance = ctx.getBean(Dance.class);
    	ctx.getBean(Dance.class);
    	ctx.getBean(Dance.class);
    	ctx.getBean(Dance.class);
    	ctx.getBean(Dance.class);
    	ctx.getBean(Dance.class);
    	//System.out.println(dance);
    }
}






