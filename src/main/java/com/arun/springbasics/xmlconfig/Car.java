package com.arun.springbasics.xmlconfig;

public class Car {
 
	String id;
	
	String cubicCapacity;
	
	String brand;
	
	public Car() {
		System.out.println(this.getClass().getSimpleName() +" class objet created");
	}

	public Car(String id, String cubicCapacity, String brand) {
		super();
		this.id = id;
		this.cubicCapacity = cubicCapacity;
		this.brand = brand;
	}

	@Override
	public String toString() {
		return "Car [id=" + id + ", cubicCapacity=" + cubicCapacity + ", brand=" + brand + "]";
	}
	
}
